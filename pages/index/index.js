//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    res:"VS",
    setInter:"",
    img:"rock",
    plImg:"rock"
  },
  //事件处理函数
  onLoad:function(){
    this.startInterval();
  },
  //npc的图片滚动
  startInterval:function(){
    //获取this对象
    var that = this;
    var i = 0;
    this.setInter = setInterval(function () {
      var npc = ["rock", "paper", "scissors"];
      //一、随机数方式
      // i = parseInt(Math.random() * 3);
      // that.setData({
      //   img: npc[i]
      // });
      // console.log(i,temp);

      //二、顺序方式
      if(i==2){
        i=0;
        that.setData({
          img: npc[i]
        });
      }else{
        i++;
        that.setData({
          img: npc[i]
        });
      }
    }, 100);
    
  },
  //点击选项触发结果
  clickRes:function(e){
    //清除定时器
    clearInterval(this.setInter);
    //获取点击选项
    var img = e.currentTarget.dataset.player;
    //设置结果
    this.setData({
      plImg:img
    });
    //判断结果
    if(this.data.img==this.data.plImg){
      this.setData({
        res:"居然打平了，请再加油！"
      })
    }else if(this.data.img=="rock"&&this.data.plImg=="paper"){
      this.setData({
        res: "恭喜！！！！你赢了！！"
      })
    } else if (this.data.img == "rock" && this.data.plImg == "scissors"){
      this.setData({
        res: "输了~可恶！"
      })
    } else if (this.data.img == "scissors" && this.data.plImg == "rock") {
      this.setData({
        res: "恭喜！！！！你赢了！！"
      })
    } else if (this.data.img == "scissors" && this.data.plImg == "paper") {
      this.setData({
        res: "输了~可恶！"
      })
    } else if (this.data.img == "paper" && this.data.plImg == "scissors") {
      this.setData({
        res: "恭喜！！！！你赢了！！"
      })
    } else if (this.data.img == "paper" && this.data.plImg == "rock") {
      this.setData({
        res: "输了~可恶！"
      })
    }
  },

  //重新开始
  restart:function(){
    var that = this;
    this.setInter = setInterval(function () {
      var npc = ["rock", "paper", "scissors"];
      var i = parseInt(Math.random() * 3);
      that.setData({
        res:"VS",
        img: npc[i]
      });
      // console.log(that.data.img);
    }, 100);
  }
})
